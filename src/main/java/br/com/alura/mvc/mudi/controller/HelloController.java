package br.com.alura.mvc.mudi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
	
	@GetMapping ("/hello")
	public String hello(Model model) {
		model.addAttribute("nome", "Jorge");
		model.addAttribute("outronome", "Rafaela");
		return "hello";
	}
	
	//nao importa o nome do metodo
	// o que importa é a string do mapping e o retorno da pagina (nome da view)
	@GetMapping ("/pagina")
	public String teste(Model model) {
		return "test";
	}


}
