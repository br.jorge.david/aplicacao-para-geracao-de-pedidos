# Loja Mudi
> Loja criada para exposição de produtos e geração de ofertas aos interessados.

Esta loja foi desenvolvida tendo como base o curso de Spring Boot da Alura.

<h1>Funcionalidades</h1>

-  Login/Logout do Usuário
-  Visualização de informações de acordo com o usuário logado
-  Adição/Alteração de Produtos para venda
-  Criação de Ofertas
-  Uso de cores diferentes para cada status dos pedidos
-  Criação de APIs Rest
-  Consumo de APIs Rest
-  Validação de Formulários
-  Uso de Cache para disponibilidade dos dados


<h1> Tecnologias Utilizadas</h1>

 - Bootstrap
 - Thymeleaf
 - Spring Security
 - Spring Data JPA
 - Vue.JS
 - API Rest  
 - AJAX 
 - Axiom

![](../header.png)

